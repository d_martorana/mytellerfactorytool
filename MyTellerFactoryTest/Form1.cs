﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using myArcaNameSpace;
using CM18tool;
using INI;
using System.IO;
using System.Data.OleDb;
using System.Reflection;
using AForge.Video;
using AForge.Video.DirectShow;
using AForge.Video.VFW;

namespace MyTellerFactoryTest
{
    public partial class Form1 : Form
    {
        IniFile config = new IniFile(Application.StartupPath + "\\config.ini");
        string localDBFolder;
        string databaseName;
        string connectionString;
        public OleDbConnection myConnection = new OleDbConnection();
        public OleDbCommand dbCommand;
        public OleDbDataReader dbReader;
        public DataTable dbDataTable = new DataTable();
        public string sqlCommand;
        IniFile messages;
        List<string> partNumber = new List<string>();
        List<string> description = new List<string>();
        string mySQL;
        Arca.SMyTeller myTeller = new Arca.SMyTeller();
        List<Arca.SSingleTest> myTest = new List<Arca.SSingleTest>();
        const string PASS = "PASS";
        const string FAIL = "FAIL";
        string testResult;
        CmDLLs.ConnectionParam cm18ConnectionParameter = new CmDLLs.ConnectionParam();
        short hCon;
        CmDLLs myCM = new CmDLLs();

        FilterInfoCollection elencoDispositiviInput = null;
        Dictionary<string, string> datiDispositivi = new Dictionary<string, string>();
        VideoCaptureDevice webcam = null;
        Graphics superficieScatto = null;
        Rectangle rettangoloScatto;
        enum camMode { videoMode, photoMode};
        camMode modalità = camMode.photoMode;
        bool catturaFrame = false;

        //BARCODE
        RS232 rsBarcode = new RS232();

        public Form1()
        {
            InitializeComponent();
            MyInitializeComponent();
        }
        void MyInitializeComponent()
        {
            messages = new IniFile(config.GetValue("options", "language")+".ini");
            Arca.iniMessage = messages;
            Arca.WriteMessage(lblTitle, 1);
            databaseName = config.GetValue("options", "databasename");
            localDBFolder = config.GetValue("options", "databaseFolder");
            connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;" +
                "Data Source=" + Path.Combine(localDBFolder, databaseName) +
                "; Persist Security Info=True;" + "Jet OLEDB:Database Password=;";
            myConnection.ConnectionString = connectionString;
            myConnection.Open();
            myConnection.Close();
            cm18ConnectionParameter.ConnectionMode = CmDLLs.DLINK_MODE_SERIAL;
            cm18ConnectionParameter.pRsConf.device = "COM1";
            cm18ConnectionParameter.pRsConf.baudrate = 9600;
            cm18ConnectionParameter.pRsConf.car = 8;
            cm18ConnectionParameter.pRsConf.parity = 0;
            cm18ConnectionParameter.pRsConf.stop = 1;
            myCM.CommandSent += new EventHandler(CmDataSent);
            myCM.CommandReceived += new EventHandler(CmDataReceived);
            elencoDispositiviInput = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            grpBarCode.Top = 100;
            grpBarCode.Left = 5;
            foreach (FilterInfo dispositivo in elencoDispositiviInput)
            {
                datiDispositivi.Add(dispositivo.Name, dispositivo.MonikerString);
            }

            //BARCODE
            rsBarcode.DataReceived += new EventHandler(BarcodeDataReceived);
        }
        public void CmDataSent(object sender, EventArgs e)
        {
            lstComScope.Items.Add("SND:" + sender);
            lstComScope.SetSelected(lstComScope.Items.Count - 1, true);
            this.Refresh();
        }

        public void CmDataReceived(object sender, EventArgs e)
        {
            lstComScope.Items.Add("RCV:" + sender);
            lstComScope.SetSelected(lstComScope.Items.Count - 1, true);
            this.Refresh();
        }
        string ReadDb(string SqlCmd, string fieldName)
        {
            string result = "";
            myConnection.Open();
            dbCommand = myConnection.CreateCommand();
            dbCommand.CommandText = SqlCmd;
            dbReader = dbCommand.ExecuteReader();
            while (dbReader.Read() == true)
            {
                result = dbReader[fieldName].ToString();
            }
            myConnection.Close();
            return result;
        }

        void FillComboProduct()
        {
            partNumber.Clear();
            description.Clear();
            cboPartNumber.Enabled = true;
            cboPartNumber.Items.Clear();
            cboPartNumber.Text = "";
            mySQL = "SELECT DISTINCT prodotti.id_prodotto, prodotti.descrizione, tblmyteller.id_prodotto FROM prodotti, tblmyteller WHERE prodotti.categoria_prodotto = 'myteller' AND tblmyteller.id_prodotto=prodotti.id_prodotto";
            myConnection.Open();
            dbCommand = myConnection.CreateCommand();
            dbCommand.CommandText = mySQL;
            dbReader = dbCommand.ExecuteReader();
            while (dbReader.Read() == true)
            {
                cboPartNumber.Items.Add(dbReader["prodotti.id_prodotto"].ToString());
                partNumber.Add(dbReader["prodotti.id_prodotto"].ToString());
                description.Add(dbReader["descrizione"].ToString());
            }
            myConnection.Close();
            cboPartNumber.Focus();
        }

        void MyTellerTest()
        {
            /*
                test1=Calibrazione touch screen
                    eseguire il prog IRTOUCH o BOXONE
                    avviare la calibrazione 
                test2=LS40
                    verificare la versione di fw e serial number
                    passare 2 volte il documento 'DOCUTEST--066' (300dpi - MICR - double leaf 50/145/215 - all side - clear image - doc eject -1 doc)
                        verificare la lettura
                    passare 2 volte il documento 'DOCUTEST--065' (stessa impostazione)
                        verificare la lettura
                    passare il documento 'DOCUTEST-066' doppio (impostare warnig double leafing e selezionare doc sorted)
                        verificare il warning
                    resettare la periferica
                test3=CRM8
                test4=CM18T
                test5=tastiera epp
                test6=card reader sankyio
                test7=stampante ricevute
                test8=scheda service
                test9=Scanner A4 canon
                test10=Scheda audio
                test11=Connessione LAN
                test12=Stampante A4
                test13=Scanner BANCOR
                test14=Barcode reader
                test15=modulo NFC
                test16=webcam
                test17=UPS
                test18=Tastiera epp ADA
            */
            Type type = typeof(Form1);
            for (int i = 0; i<myTest.Count; i++)
            {
                MethodInfo method = type.GetMethod(myTest[i].functionName, BindingFlags.Instance | BindingFlags.Public);

                if (method!=null)
                {
                    try
                    {
                        method.Invoke(this, null);
                    }
                    catch { testResult = FAIL; }
                    if (testResult != PASS)
                        goto endTest;
                }

            }
            endTest:
            if (testResult == PASS)
            {
                Arca.WriteMessage(lblMessage, 4);//TEST ESEGUITO CORRETTAMENTE. Premi un tasto per continuare
                // credenziali server??
            }
            else
            {
                Arca.WriteMessage(lblMessage, 5); //TEST FALLITO. Premi un tasto per continuare
            }
        }

        public string Taratura08815026()
        {
            testResult = PASS;
            return testResult;
        }

        public string TestLettBad0009()
        {
            testResult = PASS;
            return testResult;
        }
    
        public string TestPrinterCustom()
        {
            testResult = PASS;
            return testResult;
        }

        public string TestPrinterCustomA4()
        {
            testResult = PASS;
            return testResult;
        }

        public string TestTastieraEpp()
        {
            testResult = PASS;
            return testResult;
        }

        public string TestLs40()
        {
            testResult = PASS;
            return testResult;
        }

        public string TestScannerA4Canon()
        {
            testResult = PASS;
            return testResult;
        }

        public string TestScannerA4Bancor()
        {
            testResult = PASS;
            return testResult;
        }

        public string TestWebcam()
        {
            testResult = PASS;
            return testResult;
        }

        public string TestBarcode08813020()
        {
            grpBarCode.Visible = true;
            int timeOut = 50000;
            testResult = PASS;
            rsBarcode.ConfigCom(config.GetValue("connections", "barcodeport"));
            rsBarcode.OpenCom();
            Arca.WriteMessage(lblMessage, 6); //Leggere tutti i barcode del DOCT-R112
            letturaBarcode:
            rsBarcode.commandAnswer = "";
            rsBarcode.WaitingForData(timeOut);
            switch (rsBarcode.commandAnswer.Substring(0,9))
            {
                case "Test bcKO":
                    testResult = FAIL;
                    goto fineTest;
                case "Test  BC2":
                    lbl2D.Text = rsBarcode.commandAnswer;
                    lblE2D.Text = "FAIL";
                    lblE2D.ForeColor = Color.Red;
                    if (lblA2D.Text == lbl2D.Text)
                    {
                        lblE2D.Text = "PASS";
                        lblE2D.ForeColor = Color.Green;
                    }
                    break;
                case "Test QR C":
                    lblQR.Text = rsBarcode.commandAnswer;
                    lblEQR.Text = "FAIL";
                    lblEQR.ForeColor = Color.Red;
                    if (lblAQR.Text == lblQR.Text)
                    {
                        lblEQR.Text = "PASS";
                        lblEQR.ForeColor = Color.Green;
                    }
                    break;
                case "Test Code":
                    lblCo.Text = rsBarcode.commandAnswer;
                    lblECo.Text = "FAIL";
                    lblECo.ForeColor = Color.Red;
                    if (lblACo.Text == lblCo.Text)
                    {
                        lblECo.Text = "PASS";
                        lblECo.ForeColor = Color.Green;
                    }
                    break;
                case "Test DMTR":
                    lblDM.Text = rsBarcode.commandAnswer;
                    lblEDM.Text = "FAIL";
                    lblEDM.ForeColor = Color.Red;
                    if (lblADM.Text == lblDM.Text)
                    {
                        lblEDM.Text = "PASS";
                        lblEDM.ForeColor = Color.Green;
                    }
                    break;
                case "Test PDF4":
                    lblPD.Text = rsBarcode.commandAnswer;
                    lblEPD.Text = "FAIL";
                    lblEPD.ForeColor = Color.Red;
                    if (lblAPD.Text == lblPD.Text)
                    {
                        lblEPD.Text = "PASS";
                        lblEPD.ForeColor = Color.Green;
                    }
                    break;
                default:
                    MessageBox.Show(rsBarcode.commandAnswer, "ERRORE LETTURA - BARCODE INATTESO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    break;
            }
            if (lblE2D.Text == "PASS" && lblECo.Text == "PASS" && lblEDM.Text == "PASS" && lblEPD.Text == "PASS" && lblEQR.Text=="PASS")
            {
                goto fineTest;
            }
            goto letturaBarcode;
            fineTest:
            rsBarcode.CloseCom();
            grpBarCode.Visible = false;
            return testResult;
        }

        public string TestNfc()
        {
            testResult = PASS;
            return testResult;
        }

        public string TestAudio()
        {
            testResult = PASS;
            return testResult;
        }

        public string TestUpsRiello()
        {
            testResult = PASS;
            return testResult;
        }

        public string TestCm18()
        {
            testResult = PASS;
            int reply = -1;
            reply = myCM.CMConnect(ref cm18ConnectionParameter);
            myCM.CMSingleCommand("T,1,0,88");
            reply = myCM.CMDisconnect();
            return testResult;
        }

        public string TestCoin()
        {
            testResult = PASS;
            return testResult;
        }

        
        private void Form1_Load(object sender, EventArgs e)
        {
            //progressBar1.ForeColor = Color.Red;
            FillComboProduct();
            Arca.WriteMessage(lblMessage, 2); //Seleziona il part number e premi start per avviare il collaudo
        }

        private void cboPartNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblInfo.Text = partNumber[cboPartNumber.SelectedIndex] + " - " + description[cboPartNumber.SelectedIndex];
            if (cboPartNumber.SelectedIndex > -1)
            {
                FillTestList();
                btnStart.Enabled = true;
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            cboPartNumber.Visible = false;
            btnStart.Visible = false;
            txtSerialNumber.Top = 134;
            txtSerialNumber.Visible = true;
            Arca.WriteMessage(lblMessage, 3); //Inserisci il serial number e premi INVIO
            txtSerialNumber.Focus();
        }
        
        void FillTestList()
        {
            string test;
            Arca.SSingleTest tempTest = new Arca.SSingleTest();
            int testIndex = 0;
            mySQL = "SELECT * FROM tblmyteller WHERE id_prodotto = '" + cboPartNumber.Text + "'";
            myConnection.Open();
            dbCommand = myConnection.CreateCommand();
            dbCommand.CommandText = mySQL;
            dbReader = dbCommand.ExecuteReader();
            while (dbReader.Read() == true)
            {
                myTeller.partNumber = (dbReader["id_prodotto"].ToString());
                myTeller.monitorModel = (dbReader["monitormodel"].ToString());
                myTeller.cardreaderModel = (dbReader["cardreadermodel"].ToString());
                myTeller.printerModel = (dbReader["printermodel"].ToString());
                myTeller.printerA4Model = (dbReader["printera4model"].ToString());
                myTeller.EPPModel = (dbReader["eppmodel"].ToString());
                myTeller.scannerModel = (dbReader["scannermodel"].ToString());
                myTeller.scannerA4Model = (dbReader["scannera4model"].ToString());
                myTeller.webcamModel = (dbReader["webcammodel"].ToString());
                myTeller.barcodereaderModel = (dbReader["barcodereadermodel"].ToString());
                myTeller.NFCModel = (dbReader["nfcmodel"].ToString());
                myTeller.AudioTest = (dbReader["testaudio"].ToString());
                myTeller.UPSModel = (dbReader["upsmodel"].ToString());
                myTeller.CMModel = (dbReader["cmmodel"].ToString());
                myTeller.coinModel = (dbReader["coinmodel"].ToString());
            }
            myConnection.Close();

            myTest = new List<Arca.SSingleTest>();
            test = "touchscreen";
            tempTest.description = config.GetValue(test, "description");
            switch (myTeller.monitorModel.ToUpper())
            {
                case "08815026":
                    tempTest.functionName = config.GetValue(test, config.GetValue(test, "model"));
                    tempTest.testID = testIndex;
                    testIndex += 1;
                    myTest.Add(tempTest);
                    break;
                default:
                    break;
            }

            test = "cardreader";
            tempTest.description = config.GetValue(test, "description");
            switch (myTeller.cardreaderModel.ToUpper())
            {
                case "LETT-BAD-0009":
                    tempTest.functionName = config.GetValue(test, config.GetValue(test, "model"));
                    tempTest.testID = testIndex;
                    testIndex += 1;
                    myTest.Add(tempTest);
                    break;
                default:
                    break;
            }

            test = "printer";
            tempTest.description = config.GetValue(test, "description");
            switch (myTeller.printerModel.ToUpper())
            {
                case "CUSTOM":
                    tempTest.functionName = config.GetValue(test, config.GetValue(test, "model"));
                    tempTest.testID = testIndex;
                    testIndex += 1;
                    myTest.Add(tempTest);
                    break;
                default:
                    break;
            }

            test = "printerA4";
            tempTest.description = config.GetValue(test, "description");
            switch (myTeller.printerA4Model.ToUpper())
            {
                case "CUSTOM":
                    tempTest.functionName = config.GetValue(test, config.GetValue(test, "model"));
                    tempTest.testID = testIndex;
                    testIndex += 1;
                    myTest.Add(tempTest);
                    break;
                default:
                    break;
            }

            test = "tastieraepp";
            tempTest.description = config.GetValue(test, "description");
            switch (myTeller.EPPModel.ToUpper())
            {
                case "ZT598":
                    tempTest.functionName = config.GetValue(test, config.GetValue(test, "model"));
                    tempTest.testID = testIndex;
                    testIndex += 1;
                    myTest.Add(tempTest);
                    break;
                default:
                    break;
            }

            test = "scanner";
            tempTest.description = config.GetValue(test, "description");
            switch (myTeller.scannerModel.ToUpper())
            {
                case "LS40 OEM":
                    tempTest.functionName = config.GetValue(test, config.GetValue(test, "model"));
                    tempTest.testID = testIndex;
                    testIndex += 1;
                    myTest.Add(tempTest);
                    break;
                default:
                    break;
            }

            test = "scannerA4";
            tempTest.description = config.GetValue(test, "description");
            switch (myTeller.scannerA4Model.ToUpper())
            {
                case "CANON":
                    tempTest.functionName = config.GetValue(test, config.GetValue(test, "model"));
                    tempTest.testID = testIndex;
                    testIndex += 1;
                    myTest.Add(tempTest);
                    break;
                case "BANCOR":
                    tempTest.functionName = config.GetValue(test, config.GetValue(test, "model"));
                    tempTest.testID = testIndex;
                    testIndex += 1;
                    myTest.Add(tempTest);
                    break;
                default:
                    break;
            }

            test = "webcam";
            tempTest.description = config.GetValue(test, "description");
            switch (myTeller.webcamModel.ToUpper())
            {
                case "SI":
                    tempTest.functionName = config.GetValue(test, config.GetValue(test, "model"));
                    tempTest.testID = testIndex;
                    testIndex += 1;
                    myTest.Add(tempTest);
                    break;
                default:
                    break;
            }

            test = "barcodereader";
            tempTest.description = config.GetValue(test, "description");
            switch (myTeller.barcodereaderModel.ToUpper())
            {
                case "08813020":
                    tempTest.functionName = config.GetValue(test, config.GetValue(test, "model"));
                    tempTest.testID = testIndex;
                    testIndex += 1;
                    myTest.Add(tempTest);
                    break;
                default:
                    break;
            }

            test = "nfc";
            tempTest.description = config.GetValue(test, "description");
            switch (myTeller.NFCModel.ToUpper())
            {
                case "SI":
                    tempTest.functionName = config.GetValue(test, config.GetValue(test, "model"));
                    tempTest.testID = testIndex;
                    testIndex += 1;
                    myTest.Add(tempTest);
                    break;
                default:
                    break;
            }

            test = "audio";
            tempTest.description = config.GetValue(test, "description");
            switch (myTeller.AudioTest.ToUpper())
            {
                case "SI":
                    tempTest.functionName = config.GetValue(test, config.GetValue(test, "model"));
                    tempTest.testID = testIndex;
                    testIndex += 1;
                    myTest.Add(tempTest);
                    break;
                default:
                    break;
            }

            test = "ups";
            tempTest.description = config.GetValue(test, "description");
            switch (myTeller.UPSModel.ToUpper())
            {
                case "RIELLO":
                    tempTest.functionName = config.GetValue(test, config.GetValue(test, "model"));
                    tempTest.testID = testIndex;
                    testIndex += 1;
                    myTest.Add(tempTest);
                    break;
                default:
                    break;
            }

            test = "recycler";
            tempTest.description = config.GetValue(test, "description");
            switch (myTeller.CMModel.ToUpper())
            {
                case "SI":
                    tempTest.functionName = config.GetValue(test, config.GetValue(test, "model"));
                    tempTest.testID = testIndex;
                    testIndex += 1;
                    myTest.Add(tempTest);
                    break;
                default:
                    break;
            }

            test = "coin";
            tempTest.description = config.GetValue(test, "description");
            switch (myTeller.coinModel.ToUpper())
            {
                case "SI":
                    tempTest.functionName = config.GetValue(test, config.GetValue(test, "model"));
                    tempTest.testID = testIndex;
                    testIndex += 1;
                    myTest.Add(tempTest);
                    break;
                default:
                    break;
            }

            removeLabel:
            foreach (Control lbl in pnlTest.Controls)
            {
                if (lbl is Label && lbl.Name != "lblTestList")
                {
                    pnlTest.Controls.Remove(lbl);
                }
            }
            if (pnlTest.Controls.Count > 1)
                goto removeLabel;
            List<Label> lblTest = new List<Label>();
            for (int i = 0; i < myTest.Count; i++)
            {
                lblTest.Add(new Label());
                pnlTest.Controls.Add(lblTest[i]);
                lblTest[i].Font = lblTestList.Font;
                lblTest[i].AutoSize = true;
                lblTest[i].Height = 13;
                lblTest[i].BackColor = Color.Red;
                lblTest[i].ForeColor = Color.Black;
                lblTest[i].Text = myTest[i].description.ToUpper();
                lblTest[i].Location = new Point(0, (i * 25) + 25);
                lblTest[i].Name = "lblTestNumber" + i;
                lblTest[i].Tag = myTest[i].testID;
            }
        }

        private void txtSerialNumber_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyValue==13)
            {
                myTeller.serialNumber = txtSerialNumber.Text;
                txtSerialNumber.Visible = false;
                MyTellerTest();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            catturaFrame = true;
            if (webcam != null)
            {
                if (webcam.IsRunning)
                    webcam.Stop();
                webcam = null;
            }
            superficieScatto = picWebCam.CreateGraphics();
            rettangoloScatto = picWebCam.ClientRectangle;
            webcam = new VideoCaptureDevice(datiDispositivi[elencoDispositiviInput[0].Name]);
            webcam.NewFrame += new NewFrameEventHandler(NuovoFotogramma);
            webcam.Start();
            
        }
        void NuovoFotogramma(object sender, NewFrameEventArgs e)
        {
            if (modalità == camMode.photoMode)
            {
                if (catturaFrame)
                {
                    superficieScatto.DrawImage(e.Frame, rettangoloScatto);
                    catturaFrame = false;
                }
            }
            else
            {
                superficieScatto.DrawImage(e.Frame, rettangoloScatto);
            }
            //
        }

        private void btnFotoVideo_Click(object sender, EventArgs e)
        {
            if (modalità == camMode.photoMode)
                modalità = camMode.videoMode;
            else
                modalità = camMode.photoMode;

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            button1.Enabled = false;
            MessageBox.Show(TestBarcode08813020());
            button1.Enabled = true;
        }
        public void BarcodeDataSent(object sender, EventArgs e)
        {
            lstComScope.Items.Add("SND-BcR:" + rsBarcode.commandSent);
        }
        public void BarcodeDataReceived(object sender, EventArgs e)
        {
            lstComScope.Items.Add("RCV-BcR:" + rsBarcode.commandAnswer);
        }

        private void btnBcFail_Click(object sender, EventArgs e)
        {
            rsBarcode.commandAnswer = "Test bcKO";
        }
    }

    

}
