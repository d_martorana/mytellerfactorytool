﻿namespace MyTellerFactoryTest
{
    partial class Form1
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.pnlLogo = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.pnlTest = new System.Windows.Forms.Panel();
            this.lblTestList = new System.Windows.Forms.Label();
            this.pnlComScope = new System.Windows.Forms.Panel();
            this.lstComScope = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.pnlTitle = new System.Windows.Forms.Panel();
            this.lblTitle = new System.Windows.Forms.Label();
            this.splitContainer5 = new System.Windows.Forms.SplitContainer();
            this.pnlMessage = new System.Windows.Forms.Panel();
            this.btnFotoVideo = new System.Windows.Forms.Button();
            this.picWebCam = new System.Windows.Forms.PictureBox();
            this.btnScatta = new System.Windows.Forms.Button();
            this.txtSerialNumber = new System.Windows.Forms.TextBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.cboPartNumber = new System.Windows.Forms.ComboBox();
            this.lblMessage = new System.Windows.Forms.Label();
            this.pnlInfo = new System.Windows.Forms.Panel();
            this.lblInfo = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.grpBarCode = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblAQR = new System.Windows.Forms.Label();
            this.lblA2D = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblACo = new System.Windows.Forms.Label();
            this.lblADM = new System.Windows.Forms.Label();
            this.lblAPD = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblPD = new System.Windows.Forms.Label();
            this.lblDM = new System.Windows.Forms.Label();
            this.lblCo = new System.Windows.Forms.Label();
            this.lbl2D = new System.Windows.Forms.Label();
            this.lblQR = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.lblEPD = new System.Windows.Forms.Label();
            this.lblEDM = new System.Windows.Forms.Label();
            this.lblECo = new System.Windows.Forms.Label();
            this.lblE2D = new System.Windows.Forms.Label();
            this.lblEQR = new System.Windows.Forms.Label();
            this.btnBcFail = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.pnlLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.pnlTest.SuspendLayout();
            this.pnlComScope.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            this.pnlTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).BeginInit();
            this.splitContainer5.Panel1.SuspendLayout();
            this.splitContainer5.Panel2.SuspendLayout();
            this.splitContainer5.SuspendLayout();
            this.pnlMessage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picWebCam)).BeginInit();
            this.pnlInfo.SuspendLayout();
            this.grpBarCode.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer4);
            this.splitContainer1.Size = new System.Drawing.Size(1008, 621);
            this.splitContainer1.SplitterDistance = 253;
            this.splitContainer1.TabIndex = 0;
            this.splitContainer1.TabStop = false;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.pnlLogo);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer2.Size = new System.Drawing.Size(253, 621);
            this.splitContainer2.SplitterDistance = 78;
            this.splitContainer2.TabIndex = 0;
            this.splitContainer2.TabStop = false;
            // 
            // pnlLogo
            // 
            this.pnlLogo.Controls.Add(this.pictureBox1);
            this.pnlLogo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlLogo.Location = new System.Drawing.Point(0, 0);
            this.pnlLogo.Name = "pnlLogo";
            this.pnlLogo.Size = new System.Drawing.Size(253, 78);
            this.pnlLogo.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(226, 63);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.pnlTest);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.pnlComScope);
            this.splitContainer3.Size = new System.Drawing.Size(253, 539);
            this.splitContainer3.SplitterDistance = 410;
            this.splitContainer3.TabIndex = 0;
            this.splitContainer3.TabStop = false;
            // 
            // pnlTest
            // 
            this.pnlTest.Controls.Add(this.lblTestList);
            this.pnlTest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTest.Location = new System.Drawing.Point(0, 0);
            this.pnlTest.Name = "pnlTest";
            this.pnlTest.Size = new System.Drawing.Size(253, 410);
            this.pnlTest.TabIndex = 0;
            // 
            // lblTestList
            // 
            this.lblTestList.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTestList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestList.ForeColor = System.Drawing.Color.White;
            this.lblTestList.Location = new System.Drawing.Point(0, 0);
            this.lblTestList.Name = "lblTestList";
            this.lblTestList.Size = new System.Drawing.Size(253, 20);
            this.lblTestList.TabIndex = 0;
            this.lblTestList.Text = "Test List";
            // 
            // pnlComScope
            // 
            this.pnlComScope.Controls.Add(this.lstComScope);
            this.pnlComScope.Controls.Add(this.label2);
            this.pnlComScope.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlComScope.Location = new System.Drawing.Point(0, 0);
            this.pnlComScope.Name = "pnlComScope";
            this.pnlComScope.Size = new System.Drawing.Size(253, 125);
            this.pnlComScope.TabIndex = 0;
            // 
            // lstComScope
            // 
            this.lstComScope.FormattingEnabled = true;
            this.lstComScope.HorizontalScrollbar = true;
            this.lstComScope.Location = new System.Drawing.Point(3, 22);
            this.lstComScope.Name = "lstComScope";
            this.lstComScope.Size = new System.Drawing.Size(247, 95);
            this.lstComScope.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "ComScope";
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.pnlTitle);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.splitContainer5);
            this.splitContainer4.Size = new System.Drawing.Size(751, 621);
            this.splitContainer4.SplitterDistance = 79;
            this.splitContainer4.TabIndex = 0;
            this.splitContainer4.TabStop = false;
            // 
            // pnlTitle
            // 
            this.pnlTitle.Controls.Add(this.lblTitle);
            this.pnlTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTitle.Location = new System.Drawing.Point(0, 0);
            this.pnlTitle.Name = "pnlTitle";
            this.pnlTitle.Size = new System.Drawing.Size(751, 79);
            this.pnlTitle.TabIndex = 0;
            // 
            // lblTitle
            // 
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(751, 79);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "lblTitle";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // splitContainer5
            // 
            this.splitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer5.Location = new System.Drawing.Point(0, 0);
            this.splitContainer5.Name = "splitContainer5";
            this.splitContainer5.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer5.Panel1
            // 
            this.splitContainer5.Panel1.Controls.Add(this.pnlMessage);
            // 
            // splitContainer5.Panel2
            // 
            this.splitContainer5.Panel2.Controls.Add(this.pnlInfo);
            this.splitContainer5.Size = new System.Drawing.Size(751, 538);
            this.splitContainer5.SplitterDistance = 481;
            this.splitContainer5.TabIndex = 0;
            this.splitContainer5.TabStop = false;
            // 
            // pnlMessage
            // 
            this.pnlMessage.Controls.Add(this.grpBarCode);
            this.pnlMessage.Controls.Add(this.button1);
            this.pnlMessage.Controls.Add(this.btnFotoVideo);
            this.pnlMessage.Controls.Add(this.picWebCam);
            this.pnlMessage.Controls.Add(this.btnScatta);
            this.pnlMessage.Controls.Add(this.txtSerialNumber);
            this.pnlMessage.Controls.Add(this.btnStart);
            this.pnlMessage.Controls.Add(this.cboPartNumber);
            this.pnlMessage.Controls.Add(this.lblMessage);
            this.pnlMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMessage.Location = new System.Drawing.Point(0, 0);
            this.pnlMessage.Name = "pnlMessage";
            this.pnlMessage.Size = new System.Drawing.Size(751, 481);
            this.pnlMessage.TabIndex = 0;
            // 
            // btnFotoVideo
            // 
            this.btnFotoVideo.Location = new System.Drawing.Point(577, 427);
            this.btnFotoVideo.Name = "btnFotoVideo";
            this.btnFotoVideo.Size = new System.Drawing.Size(161, 36);
            this.btnFotoVideo.TabIndex = 7;
            this.btnFotoVideo.Text = "Video/Foto";
            this.btnFotoVideo.UseVisualStyleBackColor = true;
            this.btnFotoVideo.Click += new System.EventHandler(this.btnFotoVideo_Click);
            // 
            // picWebCam
            // 
            this.picWebCam.BackColor = System.Drawing.Color.LemonChiffon;
            this.picWebCam.Location = new System.Drawing.Point(556, 212);
            this.picWebCam.Name = "picWebCam";
            this.picWebCam.Size = new System.Drawing.Size(192, 128);
            this.picWebCam.TabIndex = 6;
            this.picWebCam.TabStop = false;
            // 
            // btnScatta
            // 
            this.btnScatta.Location = new System.Drawing.Point(576, 346);
            this.btnScatta.Name = "btnScatta";
            this.btnScatta.Size = new System.Drawing.Size(163, 63);
            this.btnScatta.TabIndex = 5;
            this.btnScatta.Text = "button1";
            this.btnScatta.UseVisualStyleBackColor = true;
            this.btnScatta.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtSerialNumber
            // 
            this.txtSerialNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.txtSerialNumber.Location = new System.Drawing.Point(262, 300);
            this.txtSerialNumber.Name = "txtSerialNumber";
            this.txtSerialNumber.Size = new System.Drawing.Size(225, 29);
            this.txtSerialNumber.TabIndex = 4;
            this.txtSerialNumber.Visible = false;
            this.txtSerialNumber.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSerialNumber_KeyUp);
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.Color.LightGray;
            this.btnStart.Enabled = false;
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.btnStart.Location = new System.Drawing.Point(262, 183);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(225, 69);
            this.btnStart.TabIndex = 3;
            this.btnStart.Text = "START";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // cboPartNumber
            // 
            this.cboPartNumber.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPartNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPartNumber.FormattingEnabled = true;
            this.cboPartNumber.Location = new System.Drawing.Point(262, 134);
            this.cboPartNumber.Name = "cboPartNumber";
            this.cboPartNumber.Size = new System.Drawing.Size(225, 32);
            this.cboPartNumber.TabIndex = 2;
            this.cboPartNumber.SelectedIndexChanged += new System.EventHandler(this.cboPartNumber_SelectedIndexChanged);
            // 
            // lblMessage
            // 
            this.lblMessage.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessage.ForeColor = System.Drawing.Color.White;
            this.lblMessage.Location = new System.Drawing.Point(0, 0);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(751, 64);
            this.lblMessage.TabIndex = 1;
            this.lblMessage.Text = "lblMessage";
            this.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlInfo
            // 
            this.pnlInfo.Controls.Add(this.lblInfo);
            this.pnlInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlInfo.Location = new System.Drawing.Point(0, 0);
            this.pnlInfo.Name = "pnlInfo";
            this.pnlInfo.Size = new System.Drawing.Size(751, 53);
            this.pnlInfo.TabIndex = 0;
            // 
            // lblInfo
            // 
            this.lblInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfo.ForeColor = System.Drawing.Color.White;
            this.lblInfo.Location = new System.Drawing.Point(0, 0);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(751, 53);
            this.lblInfo.TabIndex = 1;
            this.lblInfo.Text = "lblInfo";
            this.lblInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(589, 17);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(94, 76);
            this.button1.TabIndex = 8;
            this.button1.Text = "Barcode";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // grpBarCode
            // 
            this.grpBarCode.Controls.Add(this.btnBcFail);
            this.grpBarCode.Controls.Add(this.label20);
            this.grpBarCode.Controls.Add(this.lblEPD);
            this.grpBarCode.Controls.Add(this.lblEDM);
            this.grpBarCode.Controls.Add(this.lblECo);
            this.grpBarCode.Controls.Add(this.lblE2D);
            this.grpBarCode.Controls.Add(this.lblEQR);
            this.grpBarCode.Controls.Add(this.label14);
            this.grpBarCode.Controls.Add(this.lblPD);
            this.grpBarCode.Controls.Add(this.lblDM);
            this.grpBarCode.Controls.Add(this.lblCo);
            this.grpBarCode.Controls.Add(this.lbl2D);
            this.grpBarCode.Controls.Add(this.lblQR);
            this.grpBarCode.Controls.Add(this.label13);
            this.grpBarCode.Controls.Add(this.lblAPD);
            this.grpBarCode.Controls.Add(this.lblADM);
            this.grpBarCode.Controls.Add(this.lblACo);
            this.grpBarCode.Controls.Add(this.label9);
            this.grpBarCode.Controls.Add(this.label8);
            this.grpBarCode.Controls.Add(this.label7);
            this.grpBarCode.Controls.Add(this.label6);
            this.grpBarCode.Controls.Add(this.label5);
            this.grpBarCode.Controls.Add(this.lblA2D);
            this.grpBarCode.Controls.Add(this.lblAQR);
            this.grpBarCode.Controls.Add(this.label1);
            this.grpBarCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBarCode.ForeColor = System.Drawing.Color.White;
            this.grpBarCode.Location = new System.Drawing.Point(12, 87);
            this.grpBarCode.Name = "grpBarCode";
            this.grpBarCode.Size = new System.Drawing.Size(736, 253);
            this.grpBarCode.TabIndex = 9;
            this.grpBarCode.TabStop = false;
            this.grpBarCode.Text = "Lettura DOCT-R112";
            this.grpBarCode.Visible = false;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(6, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "2D";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblAQR
            // 
            this.lblAQR.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAQR.ForeColor = System.Drawing.Color.White;
            this.lblAQR.Location = new System.Drawing.Point(130, 87);
            this.lblAQR.Name = "lblAQR";
            this.lblAQR.Size = new System.Drawing.Size(251, 20);
            this.lblAQR.TabIndex = 2;
            this.lblAQR.Text = "Test QR Code ABC-abc-1234";
            // 
            // lblA2D
            // 
            this.lblA2D.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblA2D.ForeColor = System.Drawing.Color.White;
            this.lblA2D.Location = new System.Drawing.Point(130, 57);
            this.lblA2D.Name = "lblA2D";
            this.lblA2D.Size = new System.Drawing.Size(251, 20);
            this.lblA2D.TabIndex = 3;
            this.lblA2D.Text = "Test  BC2D ABC-abc-1234";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(6, 177);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "PDF417";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(6, 147);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(106, 20);
            this.label6.TabIndex = 5;
            this.label6.Text = "Data Matrix";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(6, 117);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(106, 20);
            this.label7.TabIndex = 6;
            this.label7.Text = "Code-128";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(6, 87);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(106, 20);
            this.label8.TabIndex = 7;
            this.label8.Text = "QR code";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(6, 30);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(106, 20);
            this.label9.TabIndex = 8;
            this.label9.Text = "Tipo Barcode";
            // 
            // lblACo
            // 
            this.lblACo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblACo.ForeColor = System.Drawing.Color.White;
            this.lblACo.Location = new System.Drawing.Point(130, 117);
            this.lblACo.Name = "lblACo";
            this.lblACo.Size = new System.Drawing.Size(251, 20);
            this.lblACo.TabIndex = 9;
            this.lblACo.Text = "Test Code128 ABC-abc-1234";
            // 
            // lblADM
            // 
            this.lblADM.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblADM.ForeColor = System.Drawing.Color.White;
            this.lblADM.Location = new System.Drawing.Point(130, 147);
            this.lblADM.Name = "lblADM";
            this.lblADM.Size = new System.Drawing.Size(251, 20);
            this.lblADM.TabIndex = 10;
            this.lblADM.Text = "Test DMTRX ABC-abc-1234";
            // 
            // lblAPD
            // 
            this.lblAPD.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAPD.ForeColor = System.Drawing.Color.White;
            this.lblAPD.Location = new System.Drawing.Point(130, 177);
            this.lblAPD.Name = "lblAPD";
            this.lblAPD.Size = new System.Drawing.Size(251, 20);
            this.lblAPD.TabIndex = 11;
            this.lblAPD.Text = "Test PDF417 ABC-abc-1234";
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(130, 30);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(251, 20);
            this.label13.TabIndex = 12;
            this.label13.Text = "ATTESO";
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(387, 30);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(251, 20);
            this.label14.TabIndex = 18;
            this.label14.Text = "LETTO";
            // 
            // lblPD
            // 
            this.lblPD.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPD.ForeColor = System.Drawing.Color.White;
            this.lblPD.Location = new System.Drawing.Point(387, 177);
            this.lblPD.Name = "lblPD";
            this.lblPD.Size = new System.Drawing.Size(251, 20);
            this.lblPD.TabIndex = 17;
            // 
            // lblDM
            // 
            this.lblDM.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDM.ForeColor = System.Drawing.Color.White;
            this.lblDM.Location = new System.Drawing.Point(387, 147);
            this.lblDM.Name = "lblDM";
            this.lblDM.Size = new System.Drawing.Size(251, 20);
            this.lblDM.TabIndex = 16;
            // 
            // lblCo
            // 
            this.lblCo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCo.ForeColor = System.Drawing.Color.White;
            this.lblCo.Location = new System.Drawing.Point(387, 117);
            this.lblCo.Name = "lblCo";
            this.lblCo.Size = new System.Drawing.Size(251, 20);
            this.lblCo.TabIndex = 15;
            // 
            // lbl2D
            // 
            this.lbl2D.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl2D.ForeColor = System.Drawing.Color.White;
            this.lbl2D.Location = new System.Drawing.Point(387, 57);
            this.lbl2D.Name = "lbl2D";
            this.lbl2D.Size = new System.Drawing.Size(251, 20);
            this.lbl2D.TabIndex = 14;
            // 
            // lblQR
            // 
            this.lblQR.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQR.ForeColor = System.Drawing.Color.White;
            this.lblQR.Location = new System.Drawing.Point(387, 87);
            this.lblQR.Name = "lblQR";
            this.lblQR.Size = new System.Drawing.Size(251, 20);
            this.lblQR.TabIndex = 13;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(651, 29);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(74, 20);
            this.label20.TabIndex = 24;
            this.label20.Text = "ESITO";
            // 
            // lblEPD
            // 
            this.lblEPD.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEPD.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lblEPD.Location = new System.Drawing.Point(651, 176);
            this.lblEPD.Name = "lblEPD";
            this.lblEPD.Size = new System.Drawing.Size(74, 20);
            this.lblEPD.TabIndex = 23;
            this.lblEPD.Text = "-";
            // 
            // lblEDM
            // 
            this.lblEDM.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEDM.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lblEDM.Location = new System.Drawing.Point(651, 146);
            this.lblEDM.Name = "lblEDM";
            this.lblEDM.Size = new System.Drawing.Size(74, 20);
            this.lblEDM.TabIndex = 22;
            this.lblEDM.Text = "-";
            // 
            // lblECo
            // 
            this.lblECo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblECo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lblECo.Location = new System.Drawing.Point(651, 116);
            this.lblECo.Name = "lblECo";
            this.lblECo.Size = new System.Drawing.Size(74, 20);
            this.lblECo.TabIndex = 21;
            this.lblECo.Text = "-";
            // 
            // lblE2D
            // 
            this.lblE2D.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblE2D.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lblE2D.Location = new System.Drawing.Point(651, 56);
            this.lblE2D.Name = "lblE2D";
            this.lblE2D.Size = new System.Drawing.Size(74, 20);
            this.lblE2D.TabIndex = 20;
            this.lblE2D.Text = "-";
            // 
            // lblEQR
            // 
            this.lblEQR.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEQR.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lblEQR.Location = new System.Drawing.Point(651, 86);
            this.lblEQR.Name = "lblEQR";
            this.lblEQR.Size = new System.Drawing.Size(74, 20);
            this.lblEQR.TabIndex = 19;
            this.lblEQR.Text = "-";
            // 
            // btnBcFail
            // 
            this.btnBcFail.ForeColor = System.Drawing.Color.Black;
            this.btnBcFail.Location = new System.Drawing.Point(614, 205);
            this.btnBcFail.Name = "btnBcFail";
            this.btnBcFail.Size = new System.Drawing.Size(92, 37);
            this.btnBcFail.TabIndex = 25;
            this.btnBcFail.Text = "TEST KO";
            this.btnBcFail.UseVisualStyleBackColor = true;
            this.btnBcFail.Click += new System.EventHandler(this.btnBcFail_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1008, 621);
            this.Controls.Add(this.splitContainer1);
            this.MinimumSize = new System.Drawing.Size(1024, 600);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.pnlLogo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.pnlTest.ResumeLayout(false);
            this.pnlComScope.ResumeLayout(false);
            this.pnlComScope.PerformLayout();
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.pnlTitle.ResumeLayout(false);
            this.splitContainer5.Panel1.ResumeLayout(false);
            this.splitContainer5.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).EndInit();
            this.splitContainer5.ResumeLayout(false);
            this.pnlMessage.ResumeLayout(false);
            this.pnlMessage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picWebCam)).EndInit();
            this.pnlInfo.ResumeLayout(false);
            this.grpBarCode.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Panel pnlLogo;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.Panel pnlTest;
        private System.Windows.Forms.Panel pnlComScope;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.Panel pnlTitle;
        private System.Windows.Forms.SplitContainer splitContainer5;
        private System.Windows.Forms.Panel pnlMessage;
        private System.Windows.Forms.Panel pnlInfo;
        private System.Windows.Forms.Label lblTestList;
        private System.Windows.Forms.ListBox lstComScope;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.ComboBox cboPartNumber;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.TextBox txtSerialNumber;
        private System.Windows.Forms.Button btnScatta;
        private System.Windows.Forms.PictureBox picWebCam;
        private System.Windows.Forms.Button btnFotoVideo;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox grpBarCode;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblAPD;
        private System.Windows.Forms.Label lblADM;
        private System.Windows.Forms.Label lblACo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblA2D;
        private System.Windows.Forms.Label lblAQR;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label lblEPD;
        private System.Windows.Forms.Label lblEDM;
        private System.Windows.Forms.Label lblECo;
        private System.Windows.Forms.Label lblE2D;
        private System.Windows.Forms.Label lblEQR;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblPD;
        private System.Windows.Forms.Label lblDM;
        private System.Windows.Forms.Label lblCo;
        private System.Windows.Forms.Label lbl2D;
        private System.Windows.Forms.Label lblQR;
        private System.Windows.Forms.Button btnBcFail;
    }
}

